#!/bin/bash

export PATH=$PATH:~/android-studio/jre/bin/
export PATH=$PATH:~/android-studio-sdk/platform-tools/
export ANDROID_SERIAL=emulator-5556

if [ "$0" != "$BASH_SOURCE" ]; then
    printinfo
    export PS1="(Eloy) ${PS1}"
    return 0
fi

case "$1" in
build)
    ./gradlew build
    ;;
clean)
    ./gradlew clean
    ;;
stop)
    ./gradlew --stop
    ;;
redeploy)
    apkbuild="app-debug.apk"
    adb install -r app/build/outputs/apk/debug/${apkbuild}
    ;;
deploy)
    apkbuild="app-debug.apk"
    adb install -r app/build/outputs/apk/debug/${apkbuild}
    ;;
remove)
    adb uninstall uk.co.eloy.beta.mock
    ;;
*)
    echo "USAGE: $0 {build|clean|deploy|remove|stop} .."
esac

