package uk.co.eloy.beta.mock;

import android.location.Location;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationCallback;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.List;

import wendu.dsbridge.DWebView;
import wendu.dsbridge.OnReturnValue;
import wendu.dsbridge.CompletionHandler;


public class MainActivity extends AppCompatActivity
    {
    private FusedLocationProviderClient gps;
    private HandlerGPS handlerGPS;
    private DWebView dwebView;

    class HandlerGPS extends LocationCallback
        {
        private DWebView webview;

        public HandlerGPS(DWebView webview)
            {
            super();
            this.webview = webview;
            }

        @Override
        public void onLocationResult(LocationResult locationResult)
            {
            Location pos = locationResult.getLastLocation();
            //Log.v("Mock","onLocationResult(): " + pos.getLatitude() + " " + pos.getLongitude());
            this.webview.callHandler(
                "updateGPS",
                new Object[] { pos.getLatitude(), pos.getLongitude() },
                new OnReturnValue<Integer>() {
                    @Override
                    public void onValue(Integer retValue) {}
                    }
                );
            }
        }

    private void initGPS()
        {
        int result = ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.ACCESS_FINE_LOCATION);
        if( result != android.content.pm.PackageManager.PERMISSION_GRANTED)
            {
            String[] listPerms = new String[] {
                android.Manifest.permission.ACCESS_FINE_LOCATION
            };
            ActivityCompat.requestPermissions(this, listPerms, 1);
            }
        else
            {
            this.setupGPS();
            }
        }

    @Override
    public void onRequestPermissionsResult(
        int reqCode, String[] perms, int[] results)
        {
        if(reqCode == 1 && results.length > 0 && results[0] == 0)
            {
            this.setupGPS();
            }
        }

    private void setupGPS()
        {
        LocationRequest reqGPS = LocationRequest.create();
        reqGPS.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        reqGPS.setInterval(10 * 1000);
        this.handlerGPS = new HandlerGPS(this.dwebView);
        Task<Void> t = gps.requestLocationUpdates(reqGPS, this.handlerGPS, null);
        //FIXME: No idea why t.isSuccessful() returns false..
        }

    private void stopGPS()
        {
        if( this.handlerGPS != null )
            {
            gps.removeLocationUpdates(this.handlerGPS);
            this.handlerGPS = null;
            }
        }

    public class JavascriptBridge
        {
        private MainActivity actMain;
        public JavascriptBridge(MainActivity main)
            {
            this.actMain = main;
            }

        //for asynchronous invocation
        @JavascriptInterface
        public void testAsyn(Object msg, CompletionHandler handler)
            {
            handler.complete(msg+" [ asyn call]");
            }

        @JavascriptInterface
        public int register(Object param)
            {
            Log.v("Mock","Register(" + param + ")");
            String mode = (String)param;
            if( mode.equals("gps") )
                {
                this.actMain.initGPS();
                return 1;
                }
            else
                return 0;
            }

        @JavascriptInterface
        public int remove(Object param)
            {
            String mode = (String)param;
            if( mode.equals("gps") )
                {
                this.actMain.stopGPS();
                return 1;
                }
            else
                return 0;
            }
        }


    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.v("Mock","test");
        this.dwebView= (DWebView) findViewById(R.id.ctrlHTML);
        this.dwebView.getSettings().setJavaScriptEnabled(true);
        this.dwebView.addJavascriptObject(new JavascriptBridge(this), null);
        this.dwebView.loadData(this.loadAssets(), "text/html; charset=utf-8", "UTF-8");
        Log.v("Mock","done");

        this.gps = LocationServices.getFusedLocationProviderClient(this);
        this.handlerGPS = null;
        }

    public void clickFetch(android.view.View v)
        {
        Log.v("Mock","click");
        this.dwebView.callHandler("addValue",new Object[]{3,4},new OnReturnValue<Integer>(){
            @Override
            public void onValue(Integer retValue)
                {
                Log.v("Mock","call succeed,return value is "+retValue);
                }
            });
        }

    private String loadAsset(String name)
        { 
        StringBuffer strHTML = new StringBuffer();
        try
            {
            BufferedReader reader = new java.io.BufferedReader(
                new java.io.InputStreamReader(getAssets().open(name)));
            char[] buffer = new char[1024];
            int count;
            while( (count=reader.read(buffer)) != -1)
                {
                String strChunk = new String(buffer, 0, count);
                strHTML.append( strChunk );
                }

            reader.close();
            }
        catch (java.io.IOException e)
            {
            return "";
            }
        return strHTML.toString();
        }

    private String loadAssets()
        {
        StringBuffer html = new StringBuffer();
        html.append("<html><head>\n");
        html.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
        html.append("<script type=\"text/javascript\">\n");
        html.append( loadAsset("dsbridge.js" ) );
        html.append( loadAsset("eloy.js" ) );
        html.append( loadAsset("partner.js" ) );
        html.append("</script>\n");
        html.append("<style type=\"text/css\">\n");
        html.append( loadAsset("partner.css" ) );
        html.append("</style>\n");
        html.append("</head><body>\n");
        html.append( loadAsset("partner.html" ) );
        html.append("</body></html>\n");
        return html.toString();
        }
    }

